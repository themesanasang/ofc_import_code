﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Data;

namespace ofc_import_code
{
    class Classdb
    {
        private MySqlConnection connection;
        private string serverXP;
        private string databaseXP;
        private string uidXP;
        private string passwordXP;
        private string portXP;
        string connectionString;
        static string DESKey = "AQWkEDRF";
        static string DESIV = "HGFkDCBA";

        //Constructor     
        public Classdb()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            try
            {
                string hosxpjson = string.Format("{0}\\hosxp_config.json", Path.GetDirectoryName(Application.LocalUserAppDataPath));
                using (StreamReader r = new StreamReader(hosxpjson))
                {
                    string jsonAll = r.ReadToEnd();
                    dynamic array = JsonConvert.DeserializeObject(jsonAll);
                    foreach (var item in array)
                    {
                        serverXP = item.server;
                        databaseXP = item.dbname;
                        uidXP = item.user;
                        passwordXP = Decrypt(Convert.ToString(item.pass));
                        portXP = item.port;

                        connectionString = "";
                        connectionString = "SERVER=" + serverXP + ";" + "Port=" + portXP + ";" + "DATABASE=" +
                        databaseXP + ";" + "UID=" + uidXP + ";" + "PASSWORD=" + passwordXP + ";";

                        connection = new MySqlConnection(connectionString);
                    }
                }
            }
            catch
            {
                //MessageBox.Show("ไม่สามารถโหลดการเชื่อมต่อได้ กรุณาตรวจสอบการเชื่อมต่อ");
            }
        }

        //open connectionImg
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("ไม่สามารถติดต่อฐานข้อมูลได้.  กรุณาแจ้งผู้ดูแลระบบครับ");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, กรุณาแจ้งผู้ดูแลระบบครับ");
                        break;
                }
                return false;
            }
        }
        //Close connectionImg
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public string getHN(string pid)
        {
            string hn = "";
            MySqlCommand cmd = new MySqlCommand("select hn from patient where cid='" + pid + "' ", connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                hn = dataReader["hn"].ToString();
            }

            dataReader.Close();

            return hn;
        }

        public string getCountVN_Ovst(string vstdate, string hn)
        {
            string ck = "";
            MySqlCommand cmd = new MySqlCommand("select count(vn) as numvn from ovst where hn='" + hn + "' and vstdate='" + vstdate + "' ", connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                ck = dataReader["numvn"].ToString();
            }

            dataReader.Close();

            return ck;
        }

        public string getVN_Ovst(string vstdate, string hn)
        {
            string ck = "";
            MySqlCommand cmd = new MySqlCommand("select vn from ovst where hn='" + hn + "' and vstdate='" + vstdate + "' ", connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                ck = dataReader["vn"].ToString();
            }

            dataReader.Close();

            return ck;
        }

        public string getVN_RCPT(string approval_code, string amount)
        {
            string vn = "";
           
            string sql = " select vn from rcpt_debt where sss_approval_code=" + approval_code + " and sss_amount="+ amount + " ";

            MySqlCommand cmd = new MySqlCommand(sql, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                vn = dataReader["vn"].ToString();
            }

            dataReader.Close();
            
            return vn;
        }

        public string getVN_VisitPttype(string approval_code)
        {
            string vn = "";

            string sql = " select vn from visit_pttype where claim_code=" + approval_code + " ";

            MySqlCommand cmd = new MySqlCommand(sql, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                vn = dataReader["vn"].ToString();
            }

            dataReader.Close();

            return vn;
        }

        public string checkAppCodeKtb(string vstdate, string vsttime, string pid, string approval_code)
        {
            string ck = "";
            MySqlCommand cmd = new MySqlCommand("select approval_code from nth_ktb_data where vstdate='"+ vstdate + "' and vsttime='"+ vsttime + "' and pid='"+ pid + "' and approval_code='" + approval_code + "' ", connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                ck = dataReader["approval_code"].ToString();
            }

            dataReader.Close();

            return ck;
        }

        public string checkVNOvst(string vn)
        {
            string ck = "";

            if (vn != "")
            {
                MySqlCommand cmd = new MySqlCommand("select vn from ovst where vn=" + vn + " ", connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    ck = dataReader["vn"].ToString();
                }

                dataReader.Close();
            }
            
            return ck;
        }

        public string checkVnVPT(string vn)
        {
            string ck = "";

            if (vn != "")
            {
                MySqlCommand cmd = new MySqlCommand("select vn from visit_pttype where vn=" + vn + " ", connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    ck = dataReader["vn"].ToString();
                }

                dataReader.Close();
            }
            
            return ck;
        }

        public string checkVnRCPT_Debt(string vn)
        {
            //ck=1 ไม่มี vn
            string ck = "1";

            if (vn != "")
            {
                MySqlCommand cmd = new MySqlCommand("select sss_approval_code from rcpt_debt where vn=" + vn + " ", connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    if (dataReader["sss_approval_code"].ToString() == "")
                    {
                        //ck=2 มี vn แต่ไม่มี sss_approval_code
                        ck = "2";
                    }
                    else
                    {
                        //มี sss_approval_code
                        ck = dataReader["sss_approval_code"].ToString();
                    }
                }

                dataReader.Close();
            }

            return ck;
        }

        public DataSet getVNEmtryKtb()
        {
            DataSet ds = new DataSet();
            if (OpenConnection() == true)
            {
                MySqlDataAdapter sqlDataAdapter = new MySqlDataAdapter("select * from nth_ktb_data where ((vn='') or (vn is null)) order by vstdate asc", connection);
                sqlDataAdapter.Fill(ds);

                this.CloseConnection();

                return ds;
            }
            else
            {
                return ds;
            }
        }

        public void insertData(string vstdate, string vsttime, string pid, string approval_code, string amount, string type_import)
        {            
            if (OpenConnection() == true)
            {
                amount = amount.Replace(",", "");

                string vn = "";
                vn = getVN_RCPT(approval_code, amount);

                if (vn == "")
                {
                    vn = getVN_VisitPttype(approval_code);
                }

                #region insert table nth_ktb_data  
                if (checkAppCodeKtb(vstdate, vsttime, pid, approval_code) == "" && Convert.ToDouble(amount) > 0.00)
                {
                    DateTime date = Convert.ToDateTime(vstdate);

                    if (getCountVN_Ovst(date.ToString("yyyy-MM-dd"), getHN(pid)) == "1")
                    {
                        vn = getVN_Ovst(date.ToString("yyyy-MM-dd"), getHN(pid));
                        string sql2 = "  insert into nth_ktb_data(vstdate, vsttime, pid, vn, approval_code, amount, type_import, created_date)";
                        sql2 += " values('" + date.ToString("yyyy-MM-dd") + "','" + vsttime + "','" + pid + "','" + vn + "','" + approval_code + "'," + amount + ",'" + type_import + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "')";

                        MySqlCommand cmd2 = new MySqlCommand(sql2, connection);
                        cmd2.ExecuteNonQuery();

                        string sql5 = " update rcpt_debt set sss_approval_code='" + approval_code + "', sss_amount=" + amount + " where vn= '" + vn + "' ";
                        MySqlCommand cmd5 = new MySqlCommand(sql5, connection);
                        cmd5.ExecuteNonQuery();
                    }
                    else
                    {
                        string sql2 = "  insert into nth_ktb_data(vstdate, vsttime, pid, vn, approval_code, amount, type_import, created_date)";
                        sql2 += " values('" + date.ToString("yyyy-MM-dd") + "','" + vsttime + "','" + pid + "','" + vn + "','" + approval_code + "'," + amount + ",'" + type_import + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "')";

                        MySqlCommand cmd2 = new MySqlCommand(sql2, connection);
                        cmd2.ExecuteNonQuery();
                    }
                }
                #endregion

               
                this.CloseConnection();
            }
        }

        public DataSet getDataKTB(string dateStart, string dateEnd)
        {
            DataSet ds = new DataSet();
            if (OpenConnection() == true)
            {
                MySqlDataAdapter sqlDataAdapter = new MySqlDataAdapter("SET @row_number = 0; select (@row_number:=@row_number + 1) AS num, vstdate, vsttime, pid, vn, approval_code, amount, type_import, created_date from nth_ktb_data where vstdate between '" + dateStart + "'  and '" + dateEnd + "'  order by vstdate desc, vsttime desc", connection);
                sqlDataAdapter.Fill(ds);

                this.CloseConnection();

                return ds;
            }
            else
            {
                return ds;
            }
        }


        public void saveDataAppCode(string id, string vstdate, string vsttime, string pid, string vn, string approval_code, string amount, string type_import)
        {
            if (OpenConnection() == true)
            {
                #region update table rcpt_debt & visit_pttype
                if (checkVNOvst(vn) != "")
                {
                    if (checkVnRCPT_Debt(vn) == "1")
                    {
                        //1 ไม่มี vn
                        //check table visit_pttype & update
                        if (checkVnVPT(vn) != "")
                        {
                            string sql4 = " update visit_pttype set claim_code='" + approval_code + "'  where vn= '" + vn + "' ";
                            MySqlCommand cmd4 = new MySqlCommand(sql4, connection);
                            cmd4.ExecuteNonQuery();

                            DateTime date1 = Convert.ToDateTime(vstdate);
                            if (checkAppCodeKtb(date1.ToString("yyyy-MM-dd"), vsttime, pid, approval_code) != "" && Convert.ToDouble(amount) > 0)
                            {
                                string sql5 = " update nth_ktb_data set vn='" + vn + "' where id= " + id + " ";
                                MySqlCommand cmd5 = new MySqlCommand(sql5, connection);
                                cmd5.ExecuteNonQuery();
                            }
                        }
                    }

                    if (checkVnRCPT_Debt(vn) == "2")
                    {
                        //2 มี vn แต่ไม่มี sss_approval_code
                        //update sss_approval_code in table rcpt_debt
                        string sql4 = " update rcpt_debt set sss_approval_code='" + approval_code + "', sss_amount=" + amount + " where vn= '" + vn + "' ";
                        MySqlCommand cmd4 = new MySqlCommand(sql4, connection);
                        cmd4.ExecuteNonQuery();

                        DateTime date1 = Convert.ToDateTime(vstdate);
                        if (checkAppCodeKtb(date1.ToString("yyyy-MM-dd"), vsttime, pid, approval_code) != "" && Convert.ToDouble(amount) > 0)
                        {
                            string sql5 = " update nth_ktb_data set vn='" + vn + "' where id= " + id + " ";
                            MySqlCommand cmd5 = new MySqlCommand(sql5, connection);
                            cmd5.ExecuteNonQuery();
                        }
                    }

                    if (checkVnRCPT_Debt(vn) != "2" && checkVnRCPT_Debt(vn) != "1")
                    {
                        // มี vn มี sss_approval_code
                        //update sss_approval_code in table rcpt_debt
                        string sql4 = " update rcpt_debt set sss_approval_code='" + approval_code + "', sss_amount=" + amount + " where vn= '" + vn + "' ";
                        MySqlCommand cmd4 = new MySqlCommand(sql4, connection);
                        cmd4.ExecuteNonQuery();

                        DateTime date1 = Convert.ToDateTime(vstdate);
                        if (checkAppCodeKtb(date1.ToString("yyyy-MM-dd"), vsttime, pid, approval_code) != "" && Convert.ToDouble(amount) > 0)
                        {
                            string sql5 = " update nth_ktb_data set vn='" + vn + "' where id= " + id + " ";
                            MySqlCommand cmd5 = new MySqlCommand(sql5, connection);
                            cmd5.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("ไม่มี VN: "+vn+" นี้ในระบบกรุณาตรวจสอบข้อมูลใหม่");
                }
                #endregion

                this.CloseConnection();
            }
        }

        #region  Encrypt & Decrypt
        public string Encrypt(string openText)
        {
            byte[] key;
            byte[] IV;

            byte[] inputByteArray;
            try
            {

                key = Convert2ByteArray(DESKey);

                IV = Convert2ByteArray(DESIV);

                inputByteArray = Encoding.UTF8.GetBytes(openText);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();

                MemoryStream ms = new MemoryStream(); CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);

                cs.FlushFinalBlock();

                return Convert.ToBase64String(ms.ToArray());
            }

            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public string Decrypt(string encryptedText)
        {
            byte[] key;
            byte[] IV;

            byte[] inputByteArray;
            try
            {

                key = Convert2ByteArray(DESKey);

                IV = Convert2ByteArray(DESIV);

                int len = encryptedText.Length; inputByteArray = Convert.FromBase64String(encryptedText);


                DESCryptoServiceProvider des = new DESCryptoServiceProvider();

                MemoryStream ms = new MemoryStream();

                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);

                cs.FlushFinalBlock();

                Encoding encoding = Encoding.UTF8; return encoding.GetString(ms.ToArray());
            }

            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        static byte[] Convert2ByteArray(string strInput)
        {

            int intCounter; char[] arrChar;
            arrChar = strInput.ToCharArray();

            byte[] arrByte = new byte[arrChar.Length];

            for (intCounter = 0; intCounter <= arrByte.Length - 1; intCounter++)
                arrByte[intCounter] = Convert.ToByte(arrChar[intCounter]);

            return arrByte;
        }
        #endregion


    }
}
