﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.IO;
using System.Security.Cryptography;
using ExcelDataReader;


namespace ofc_import_code
{
    public partial class Form1 : Form
    {
        Classdb db;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

            btnSaveData.Enabled = false;

            try
            {
                if (File.Exists(string.Format("{0}\\hosxp_config.json", Path.GetDirectoryName(Application.LocalUserAppDataPath)))) {
                    string hosxpjson = string.Format("{0}\\hosxp_config.json", Path.GetDirectoryName(Application.LocalUserAppDataPath));
                    using (StreamReader r = new StreamReader(hosxpjson))
                    {
                        db = new Classdb();
                        string jsonAll = r.ReadToEnd();
                        dynamic array = JsonConvert.DeserializeObject(jsonAll);
                        foreach (var item in array)
                        {
                            txt_ip.Text = item.server;
                            txt_port.Text = item.port;
                            txt_db.Text = item.dbname;
                            txt_user.Text = item.user;
                            txt_pass.Text = db.Decrypt(Convert.ToString(item.pass));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ไม่สามารถโหลดการตั้งค่าได้ กรุณาตรวจสอบการตั้งค่า :" + ex.Message); 
            }
        }


        /**
        * config app
        * */
        private void save_config_Click(object sender, EventArgs e)
        {
            if (txt_ip.Text == "" && txt_port.Text == "" && txt_db.Text == "" && txt_user.Text == "" && txt_pass.Text == ""){
                MessageBox.Show("กรุณา กรอกข้อมูลให้ครบ!!");
            }
            else{
                try {
                    db = new Classdb();
                    List<data_hosxp> _data = new List<data_hosxp>();
                    _data.Add(new data_hosxp(){
                        server = txt_ip.Text,
                        port = txt_port.Text,
                        dbname = txt_db.Text,
                        user = txt_user.Text,
                        pass = db.Encrypt(txt_pass.Text)
                    });
                    string json = JsonConvert.SerializeObject(_data.ToArray());
                    System.IO.File.WriteAllText(string.Format("{0}\\hosxp_config.json", Path.GetDirectoryName(Application.LocalUserAppDataPath)), json);

                    MessageBox.Show(":บันทึกการเชื่อมต่อเรียบร้อยแล้ว");
                }
                catch(Exception ex){
                    MessageBox.Show("ไม่สามารถบันทึกข้อมูลได้ "+ex.Message);
                }
            }
        }
       


        /**
        * select file from folder
        * */
        private void btSelectFile_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    txtPath.Text = file;
                    if (radioExcel.Checked == true || radioText.Checked == true)
                    {
                        if (radioExcel.Checked == true)
                        {
                            readDataExcle(file);
                        }

                        if (radioText.Checked == true)
                        {
                            readDataText(file);
                        }

                        MessageBox.Show("นำเข้าข้อมูลเรียบร้อย");

                    }
                    else
                    {
                        MessageBox.Show("กรุณาเลือกประเภทเอกสาร");
                    }
                }
                catch (IOException)
                {
                }
            }
        }


        /**
        * read data from excel file
        * */
        private void readDataExcle(string path)
        {
            try
            {
                using (var stream = File.Open(path, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateCsvReader(stream))
                    {

                        DataSet ds = reader.AsDataSet();

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            db = new Classdb();
                            string[] separatorsDate = { "-" };

                            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                            {
                                if (i > 0)
                                {
                                    //MessageBox.Show(ds.Tables[0].Rows[i][1].ToString()+" : "+ ds.Tables[0].Rows[i][2].ToString()+" : "+ ds.Tables[0].Rows[i][7].ToString()+" : "+ ds.Tables[0].Rows[i][13].ToString()+" : "+ ds.Tables[0].Rows[i][16].ToString());
                                    string[] dataDate = ds.Tables[0].Rows[i][1].ToString().Split(separatorsDate, StringSplitOptions.RemoveEmptyEntries);
                                    string vstdate = dataDate[2] + "/" + dataDate[1] + "/" + dataDate[0];

                                    db.insertData(vstdate, ds.Tables[0].Rows[i][2].ToString(), ds.Tables[0].Rows[i][7].ToString(), ds.Tables[0].Rows[i][16].ToString(), ds.Tables[0].Rows[i][13].ToString(), "excel");
                                }
                            }
                        }
                    }
                }

                showVisitNoVN();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /**
        * read data from text file
        * */
        private void readDataText(string path)
        {
            try
            {
                var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    db = new Classdb();
                    string[] separators = { "|" };
                    string[] separatorsDate = { "/" };
                    string line;

                    while ((line = streamReader.ReadLine()) != null)
                    {
                        //string[] data = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                        string[] data = line.Split(separators, StringSplitOptions.None);

                        int appcode = 0;
                        int edcmoney = 0;

                        appcode = data.Length - 7;
                        edcmoney = data.Length - 11;

                        string[] dataDate = data[7].ToString().Split(separatorsDate, StringSplitOptions.RemoveEmptyEntries);
                        string vstdate = dataDate[2] + "/" + dataDate[1] + "/" + dataDate[0];

                        db.insertData(vstdate, data[8].ToString(), data[11].ToString(), data[appcode].ToString(), data[edcmoney].ToString(), "text");
                    }
                }

                showVisitNoVN();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void btnGetDataEmtry_Click(object sender, EventArgs e)
        {
            showVisitNoVN();
        }

        private void showVisitNoVN()
        {
            try
            {
                db = new Classdb();
                DataSet ds = new DataSet();
                ds = db.getVNEmtryKtb();

                dataGridView1.DataSource = ds.Tables[0];

                if (ds.Tables[0].Rows.Count > 0)
                {
                    btnSaveData.Enabled = true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }


        /**
         * save data to table rcpt_debt & visit_pttype
         * */
        private void btnSaveData_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.Rows.Count > 0)
                {
                    db = new Classdb();
                    for (int i=0; i<= dataGridView1.Rows.Count - 1; i++)
                    {
                        //MessageBox.Show(dataGridView1.Rows[i].Cells[0].Value.ToString());
                        db.saveDataAppCode(
                                dataGridView1.Rows[i].Cells[0].Value.ToString(),
                                dataGridView1.Rows[i].Cells[1].Value.ToString(),
                                dataGridView1.Rows[i].Cells[2].Value.ToString(),
                                dataGridView1.Rows[i].Cells[3].Value.ToString(),
                                dataGridView1.Rows[i].Cells[4].Value.ToString(),
                                dataGridView1.Rows[i].Cells[5].Value.ToString(),
                                dataGridView1.Rows[i].Cells[6].Value.ToString(),
                                dataGridView1.Rows[i].Cells[7].Value.ToString()
                           );
                    }

                    MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        /**
        * close app
        * */
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

       
    }
}

public class data_hosxp
{
    public string server { get; set; }
    public string port { get; set; }
    public string dbname { get; set; }
    public string user { get; set; }
    public string pass { get; set; }
}